# Sample contents of Dockerfile
# Stage 1
FROM microsoft/aspnetcore-build AS builder
ARG VERSION_SUFFIX
ARG CLEAR_VERSION_SUFFIX
WORKDIR /source

# caches restore result by copying csproj file separately
COPY *.sln .
COPY src/Issues.Basis/*.csproj src/Issues.Basis/
COPY src/Issues.Contents/*.csproj src/Issues.Contents/
COPY src/Issues/*.csproj src/Issues/
COPY src/Issues.Infrastructures/*.csproj src/Issues.Infrastructures/
COPY src/Issues.WebApp/*.csproj src/Issues.WebApp/

RUN dotnet restore

# copies the rest of your code
COPY . .
RUN dotnet publish src/Issues.WebApp --output /app/ --configuration Release

# Stage 2
FROM microsoft/aspnetcore
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["dotnet", "Issues.WebApp.dll"]
EXPOSE 80

