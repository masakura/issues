﻿using System.Threading.Tasks;
using Issues.Users;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Domains
{
    public sealed class IssueBasisAutoAuthorLibraryImplTest
    {
        public IssueBasisAutoAuthorLibraryImplTest()
        {
            _mock = new Mock<IAutoAuthorLibrary>();

            _target = new IssueBasisAutoAuthorLibraryImpl(_mock.Object);
        }

        private readonly IssueBasisAutoAuthorLibraryImpl _target;
        private readonly Mock<IAutoAuthorLibrary> _mock;
        private static readonly UserName TestUser = (UserName) "testuser";
        private readonly IssueId _testIssueId = (IssueId) 15;
        private readonly User _testUser = User.Authenticated((UserId) 10, TestUser);
        private readonly ResourceType _resourceType = IssueBasisAutoAuthorLibraryImpl.ResourceType;

        [Fact]
        public async Task BindAsync()
        {
            _mock.Setup(x => x.BindAsync(It.Is<ResourceId>(p => p.Equals((ResourceId) (int) _testIssueId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.CompletedTask);

            await _target.BindAsync(_testIssueId);

            _mock.VerifyAll();
        }

        [Fact]
        public async Task FindByIssueIdAsync()
        {
            _mock.Setup(x => x.FindByResourceIdAsync(It.Is<ResourceId>(p => p.Equals((ResourceId) (int) _testIssueId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindByIssueIdAsync(_testIssueId);

            Assert.Equal(_testUser.UserName, result.UserName);
        }

        [Fact]
        public void ResourceType()
        {
            var result = IssueBasisAutoAuthorLibraryImpl.ResourceType;

            Assert.Equal((ResourceType) "issue", result);
        }
    }
}