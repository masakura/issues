﻿using System.Threading.Tasks;
using Moq;
using Xunit;
using static Issues.Domains.ForTest.IssueIdFactoryForTest;

namespace Issues.Domains
{
    public sealed class AuthoredIssueBasisRegistryImplTest
    {
        private readonly AuthoredIssueBasisRegistryImpl _target;
        private readonly Mock<IIssueBasisAutoAuthorLibrary> _mockAuthors;
        private readonly Mock<IIssueBasisRegistry> _mockBasises;
        private readonly IssueId _testIssueId = NewId(15);

        public AuthoredIssueBasisRegistryImplTest()
        {
            _mockBasises = new Mock<IIssueBasisRegistry>();
            _mockAuthors = new Mock<IIssueBasisAutoAuthorLibrary>();
            
            _target = new AuthoredIssueBasisRegistryImpl(_mockBasises.Object, _mockAuthors.Object);
        }

        [Fact]
        public async Task CreateAsync()
        {
            _mockBasises.Setup(x => x.CreateAsync())
                .Returns(Task.FromResult(_testIssueId));
            _mockAuthors.Setup(x => x.BindAsync(It.Is<IssueId>(p => p.Equals(_testIssueId))))
                .Returns(Task.CompletedTask);
            
            var result = await _target.CreateAsync();
            
            Assert.Equal(_testIssueId, result);
            
            _mockBasises.VerifyAll();
            _mockAuthors.VerifyAll();
        }
    }
}