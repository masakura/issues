﻿using Issues.Users.Domains;

namespace Issues.Users.ForTest
{
    public static class UserFactoryForTest
    {
        private static UserId NewUserId(int id)
        {
            return (UserId) id;
        }

        public static User NewAuthenticated(int id)
        {
            return User.Authenticated(NewUserId(id), NewUserName(id));
        }

        private static UserName NewUserName(int id)
        {
            return (UserName) $"user{id}";
        }
    }
}