﻿namespace Issues.Users
{
    public struct UserName
    {
        private readonly string _value;

        public UserName(string value)
        {
            _value = value;
        }

        public static UserName Anonymouse { get; } = (UserName) "anonymouse";
        public static UserName Unknown { get; } = (UserName) "unknown";

        public override string ToString()
        {
            var text = $"{_value}";

            return string.IsNullOrWhiteSpace(text) ? string.Empty : $"@{text}";
        }

        public static explicit operator string(UserName userName)
        {
            return userName._value;
        }

        public static explicit operator UserName(string userName)
        {
            return new UserName(userName);
        }
    }
}