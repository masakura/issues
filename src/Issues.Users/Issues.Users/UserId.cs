﻿namespace Issues.Users
{
    public struct UserId
    {
        private readonly int _id;

        public UserId(int id)
        {
            _id = id;
        }

        public static explicit operator int(UserId id)
        {
            return id._id;
        }

        public static explicit operator UserId(int id)
        {
            return new UserId(id);
        }

        public static UserId Empty { get; } = new UserId();
        public bool IsEmpty => _id <= 0;

        public override string ToString()
        {
            return $"{_id}";
        }
    }
}