﻿using System.Threading.Tasks;

namespace Issues.Users
{
    public interface IUserRegistrationService
    {
        Task<ErrorMessageCollection> RegisterAsync(UserName userName, Password passowrd);
        Task<ErrorMessageCollection> ValidatePasswordAsync(Password password);
        Task<ErrorMessageCollection> ValidateUserNameAsync(UserName userName);
    }
}