﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Users
{
    public sealed class UserLibraryServiceImpl : IUserLibraryService
    {
        private readonly IUserLibrary _users;

        public UserLibraryServiceImpl(IUserLibrary users)
        {
            _users = users;
        }

        public async Task<UserViewModel> FindByUserNameAsync(UserName userName)
        {
            // TODO リファクタリング
            var user = await _users.FindByUserNameAsync(userName);

            return (UserViewModel) user;
        }
    }
}