﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Issues.Users
{
    public sealed class ErrorMessageCollection : IEnumerable<ErrorMessage>
    {
        private readonly IList<ErrorMessage> _messages;

        public ErrorMessageCollection(IEnumerable<ErrorMessage> messages)
        {
            _messages = messages.ToList().AsReadOnly();
        }

        public bool HasErrors => _messages.Count > 0;
        public static ErrorMessageCollection NoError { get; } = new ErrorMessageCollection(new ErrorMessage[] { });

        public IEnumerator<ErrorMessage> GetEnumerator()
        {
            return _messages.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return string.Join("\n", _messages.ToStrings());
        }
    }
}