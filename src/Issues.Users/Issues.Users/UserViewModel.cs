﻿using Issues.Users.Domains;

namespace Issues.Users
{
    public sealed class UserViewModel
    {
        public UserViewModel(UserId id, UserName userName) : this(id, userName, true)
        {
        }

        private UserViewModel(UserId id, UserName userName, bool isAuthenticated)
        {
            Id = id;
            UserName = userName;
            IsAuthenticated = isAuthenticated;
        }

        public UserName UserName { get; }
        public bool IsAuthenticated { get; }
        public UserId Id { get; }

        public static explicit operator UserViewModel(User user)
        {
            return user == null ? null : new UserViewModel(user.Id, user.UserName, user.IsAuthenticated);
        }

        public override string ToString()
        {
            return $"{UserName}";
        }
    }
}