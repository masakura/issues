﻿using System.Collections.Generic;
using System.Linq;

namespace Issues.Users
{
    public static class ErrorMessageEnumerableExtensions
    {
        public static IEnumerable<string> ToStrings(this IEnumerable<ErrorMessage> messages)
        {
            return messages.Select(m => (string) m);
        }

        public static ErrorMessageCollection ToCollection(this IEnumerable<ErrorMessage> messages)
        {
            return new ErrorMessageCollection(messages);
        }
    }
}