﻿namespace Issues.Users
{
    public struct ErrorMessage
    {
        private readonly string _value;

        public ErrorMessage(string value)
        {
            _value = value;
        }

        public static explicit operator string(ErrorMessage value)
        {
            return value._value;
        }

        public static explicit operator ErrorMessage(string value)
        {
            return new ErrorMessage(value);
        }

        public override string ToString()
        {
            return $"{_value}";
        }
    }
}