﻿namespace Issues.Users.Domains
{
    internal sealed class AuthenticatedUser : User
    {
        internal AuthenticatedUser(UserId id, UserName userName) : base(id, userName)
        {
        }

        public override UserType Type { get; } = UserType.Authenticated;
    }
}