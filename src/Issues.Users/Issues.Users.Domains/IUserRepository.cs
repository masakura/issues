﻿using System.Threading.Tasks;

namespace Issues.Users.Domains
{
    public interface IUserRepository
    {
        Task<ErrorMessageCollection> CreateAsync(UserName userName, Password password);
        Task<ErrorMessageCollection> ValidatePasswordAsync(Password password);
        Task<ErrorMessageCollection> ValidateUserNameAsync(UserName userName);
        Task<bool> SignInAsync(UserName userName, Password password);
        Task SignOutAsync();
        Task<User> FindAsync(UserId id);
        Task<User> FindByUserNameAsync(UserName userName);
        Task<User> GetCurrentUser();
    }
}