﻿namespace Issues.Users.Domains
{
    internal sealed class UnknownUser : User
    {
        private UnknownUser() : base(UserId.Empty, UserName.Unknown)
        {
        }

        public override UserType Type { get; } = UserType.Unknown;
        
        public static UnknownUser Instance { get; } = new UnknownUser();
    }
}