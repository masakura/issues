﻿using System.Threading.Tasks;

namespace Issues.Users.Domains
{
    public sealed class UserLibraryImpl : IUserLibrary
    {
        private readonly IUserRepository _users;

        public UserLibraryImpl(IUserRepository users)
        {
            _users = users;
        }

        public async Task<User> FindAsync(UserId id)
        {
            return await _users.FindAsync(id);
        }

        public async Task<User> FindByUserNameAsync(UserName userName)
        {
            return await _users.FindByUserNameAsync(userName);
        }
    }
}