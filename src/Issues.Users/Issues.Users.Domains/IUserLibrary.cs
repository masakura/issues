﻿using System.Threading.Tasks;

namespace Issues.Users.Domains
{
    public interface IUserLibrary
    {
        Task<User> FindAsync(UserId id);
        Task<User> FindByUserNameAsync(UserName userName);
    }
}