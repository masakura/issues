﻿namespace Issues.Users.Domains
{
    public abstract class User
    {
        protected User(UserId id, UserName userName)
        {
            Id = id;
            UserName = userName;
        }

        public UserId Id { get; }
        public UserName UserName { get; }
        public abstract UserType Type { get; }

        public bool IsAuthenticated => Type == UserType.Authenticated;
        public bool IsAnonymouse => Type == UserType.Anonymouse;
        public bool IsUnknown => Type == UserType.Unknown;

        public static User Anonymouse { get; } = AnonymouseUser.Instance;
        public static User Unknown { get; } = UnknownUser.Instance;

        public static User Authenticated(UserId id, UserName userName)
        {
            return new AuthenticatedUser(id, userName);
        }

        public override string ToString()
        {
            return $"{UserName}";
        }
    }
}