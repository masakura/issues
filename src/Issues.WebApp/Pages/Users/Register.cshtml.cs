﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Issues.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages.Users
{
    public sealed class RegisterModel : PageModel
    {
        private readonly IUserRegistrationService _userRegistration;

        public RegisterModel(IUserRegistrationService userRegistration)
        {
            _userRegistration = userRegistration;
        }

        [BindProperty]
        public RegistrationUserInput Input { get; set; }

        // ReSharper disable once UnusedMember.Global
        public async Task<ActionResult> OnPostAsync()
        {
            await ValidateUserNameAsync();
            await ValidatePasswordAsync();

            if (ModelState.IsValid)
            {
                var result = await RegisterAsync();

                if (!result.HasErrors) return RedirectToPage("/Index");

                ModelState.AddModelErrors(result);
            }

            return Page();
        }

        private async Task<ErrorMessageCollection> RegisterAsync()
        {
            return await _userRegistration
                .RegisterAsync((UserName) Input.UserName, (Password) Input.Password);
        }

        private async Task ValidatePasswordAsync()
        {
            var errors = await _userRegistration.ValidatePasswordAsync((Password) Input.Password);

            if (errors.HasErrors)
                ModelState.AddModelErrors<RegisterModel>(m => m.Input.Password, errors);
        }

        private async Task ValidateUserNameAsync()
        {
            var errors = await _userRegistration.ValidateUserNameAsync((UserName) Input.UserName);

            if (errors.HasErrors)
                ModelState.AddModelErrors<RegisterModel>(m => m.Input.UserName, errors);
        }

        public sealed class RegistrationUserInput
        {
            [DisplayName("UserName")]
            [Required]
            public string UserName { get; set; }

            [DisplayName("Password")]
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [DisplayName("Confirm password")]
            [Required]
            [DataType(DataType.Password)]
            [Compare("Password")]
            public string ConfirmPassword { get; set; }
        }
    }
}