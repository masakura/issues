﻿using System.Threading.Tasks;
using Issues.Users;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages.Users
{
    public class ProfileModel : PageModel
    {
        private readonly IUserSignInService _signIn;

        public ProfileModel(IUserSignInService signIn)
        {
            _signIn = signIn;
        }

        public UserViewModel UserInfo { get; private set; }

        // ReSharper disable once UnusedMember.Global
        public async Task OnGetAsync()
        {
            UserInfo = await _signIn.GetCurrentUserAsync();
        }
    }
}