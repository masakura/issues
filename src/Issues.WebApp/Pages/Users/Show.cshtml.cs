﻿using System.Threading.Tasks;
using Issues.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages.Users
{
    public sealed class ShowModel : PageModel
    {
        private readonly IUserLibraryService _users;

        public ShowModel(IUserLibraryService users)
        {
            _users = users;
        }

        public UserViewModel UserInfo { get; private set; }

        // ReSharper disable once UnusedMember.Global
        public async Task<IActionResult> OnGetAsync(string id)
        {
            var user = await _users.FindByUserNameAsync((UserName) id);

            if (!user.IsAuthenticated) return NotFound();

            UserInfo = user;
            return Page();
        }
    }
}