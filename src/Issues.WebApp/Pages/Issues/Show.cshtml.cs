﻿using System.Threading.Tasks;

namespace Issues.WebApp.Pages.Issues
{
    public class ShowModel : IssueModel
    {
        public ShowModel(IIssueLibraryService library, IIssueRegistrationService registry) : base(library, registry)
        {
        }

        // ReSharper disable once UnusedMember.Global
        public Task OnGetAsync(int id)
        {
            return ShowAsync(id);
        }
    }
}