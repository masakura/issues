﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages.Issues
{
    public abstract class IssueModel : PageModel
    {
        private readonly IIssueLibraryService _library;
        private readonly IIssueRegistrationService _registry;
        private IssueViewModel _issue;

        protected IssueModel(IIssueLibraryService library, IIssueRegistrationService registry)
        {
            _library = library;
            _registry = registry;
        }

        [BindProperty]
        public IssueContentViewModel Input { get; set; } = new IssueContentViewModel();

        [BindProperty]
        public IssueViewModel Issue
        {
            get => _issue;
            private set
            {
                _issue = value;
                Input = _issue.Content;
            }
        }

        protected async Task ShowAsync(int id)
        {
            Issue = await _library.FindAsync((IssueId) id);
        }

        protected Task SaveAsync(int id)
        {
            return _registry.SaveAsync((IssueId) id, Input);
        }

        protected async Task<int> SubmitAsync()
        {
            return (int) await _registry.SubmitAsync(Input);
        }

        protected RedirectToPageResult RedirectToShowPage(int id)
        {
            return RedirectToPage("/Issues/Show", new {id});
        }
    }
}