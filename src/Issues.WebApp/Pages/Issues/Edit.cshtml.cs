﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Issues.WebApp.Pages.Issues
{
    public sealed class EditModel : IssueModel
    {
        public EditModel(IIssueLibraryService library, IIssueRegistrationService registry) : base(library, registry)
        {
        }

        // ReSharper disable once UnusedMember.Global
        public async Task OnGetAsync(int id)
        {
            await ShowAsync(id);
        }

        // ReSharper disable once UnusedMember.Global
        public async Task<IActionResult> OnPostAsync(int id)
        {
            if (!ModelState.IsValid) return Page();

            await SaveAsync(id);

            return RedirectToShowPage(id);
        }
    }
}