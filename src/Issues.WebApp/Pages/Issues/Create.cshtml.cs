﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Issues.WebApp.Pages.Issues
{
    public class CreateModel : IssueModel
    {
        public CreateModel(IIssueLibraryService library, IIssueRegistrationService registry) : base(library, registry)
        {
        }


        // ReSharper disable once UnusedMember.Global
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var submittedIssueId = await SubmitAsync();

            return RedirectToShowPage(submittedIssueId);
        }
    }
}