﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Issues.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages
{
    public sealed class SignInModel : PageModel
    {
        private readonly IUserSignInService _signInService;

        public SignInModel(IUserSignInService signInService)
        {
            _signInService = signInService;
        }

        [BindProperty]
        public SignInUserInput Input { get; set; }

        // ReSharper disable once UnusedMember.Global
        public async Task<IActionResult> OnPostAsync()
        {
            var errors = await _signInService.SignInAsync((UserName) Input.UserName, (Password) Input.Password);
            
            if (errors.HasErrors) ModelState.AddModelErrors(errors);

            if (ModelState.IsValid) return RedirectToPage("/Index");

            return Page();
        }

        public sealed class SignInUserInput
        {
            [DisplayName("UserName")]
            [Required]
            public string UserName { get; set; }

            [DisplayName("Password")]
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }
    }
}