using System;
using Issues.Contents.Infrastructures;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Issues.WebApp
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // ReSharper disable once UnusedMember.Global
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddIssues();
            services.AddInfrastructures(options =>
                options.UseSqlite("Data Source=database.db"));
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void Configure(IApplicationBuilder app, IServiceProvider services)
        {
            services.GetService<ILoggerFactory>()
                .AddApplicationInsights(services);

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
        }

        // ReSharper disable once UnusedMember.Global
        public void ConfigureDevelopment(IApplicationBuilder app, IServiceProvider services)
        {
            app.UseDeveloperExceptionPage();
            
            services.GetService<IInfrastructureMigrationService>()
                .EnsureCreated();
            
            Configure(app, services);
        }

        // ReSharper disable once UnusedMember.Global
        public void ConfigureStaging(IApplicationBuilder app, IServiceProvider services)
        {
            ConfigureProduction(app, services);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public void ConfigureProduction(IApplicationBuilder app, IServiceProvider services)
        {
            app.UseExceptionHandler("/Error");

            services.GetService<IInfrastructureMigrationService>()
                .Migrate();

            Configure(app, services);
        }
    }
}
