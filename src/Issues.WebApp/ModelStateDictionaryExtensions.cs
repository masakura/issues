﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Issues.Users;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Issues.WebApp
{
    public static class ModelStateDictionaryExtensions
    {
        public static void AddModelErrors(this ModelStateDictionary dictionary, IEnumerable<ErrorMessage> messages)
        {
            foreach (var message in messages)
            {
                dictionary.AddModelError(string.Empty, (string) message);
            }
        }

        public static void AddModelErrors<T>(this ModelStateDictionary dictionary, Expression<Func<T, object>> expression, IEnumerable<ErrorMessage> messages)
        {
            foreach (var message in messages)
            {
                dictionary.AddModelError(expression, (string) message);
            }
        }
    }
}