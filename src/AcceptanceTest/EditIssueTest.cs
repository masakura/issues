﻿using System.Linq;
using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class EditIssueTest : UseWebDriver
    {
        private readonly Term _term;

        public EditIssueTest()
        {
            _term = new Term();
        }

        private IssuePage Submit(string title, string description)
        {
            var creation = Shortcuts.NewIssue();
            var show = creation.Submit(new
            {
                Title = title,
                Description = description
            });
            return show;
        }

        [Fact]
        public void EditedIssueIsSameId()
        {
            var show = Submit("title1", "description1");

            var issueId = show.Id;

            var result = show
                .ClickEdit()
                .Save(new {Title = "t", Description = "d"});

            Assert.Equal((string) issueId, result.Id);
        }

        [Fact]
        public void EditIssue()
        {
            var show = Submit("title1", "description1");

            var edition = show.ClickEdit();

            var result = edition.Save(new
            {
                Title = "modified title",
                Description = "modified description"
            });

            Assert.Equal("modified title", result.Title);
            Assert.Equal("modified description", result.Description);
            Assert.Equal("less than a minute ago", result.TimeAgo);
            Assert.InRange(result.TimeAgo.Timestamp, _term.Low, _term.High);
        }

        [Fact]
        public void ListInEditedIssue()
        {
            var show = Submit("title1", "description1");

            show
                .ClickEdit()
                .Save(new {Title = "modified t", Description = "modified d"});

            var result = Shortcuts.Issues().Issues.First();

            Assert.Equal("modified t", result.Title);
        }

        [Fact]
        public void NoAffectOrders()
        {
            Submit("title1", "description1");
            Submit("title2", "description2");
            Submit("title3", "description3");

            Shortcuts.Issues()
                .Issues.Skip(1).First()
                .ClickLink()
                .ClickEdit()
                .Save(new {Title = "modified t", Description = "description t"});

            var result = Shortcuts.Issues().Issues.Take(3);

            Assert.Equal(new[]
            {
                "title3",
                "modified t",
                "title1"
            }, result.Select(row => (string) row.Title).ToArray());
        }

        [Fact]
        public void ShowEditPage()
        {
            var result = Submit("title1", "description1")
                .ClickEdit();

            Assert.Equal("title1", result.Title);
            Assert.Equal("description1", result.Description);
        }
    }
}