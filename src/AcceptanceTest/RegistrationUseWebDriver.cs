﻿using System;

namespace AcceptanceTest
{
    public abstract class RegistrationUseWebDriver : UseWebDriver
    {
        protected readonly string TestUser;
        protected readonly string Password;

        protected RegistrationUseWebDriver()
        {
            TestUser = Guid.NewGuid().ToString();
            Password = "Hogehoge1_";

            RegisterTestUser();
        }

        private void RegisterTestUser()
        {
            RegisterTestUser(TestUser);
        }

        protected void RegisterTestUser(string userName)
        {
            Shortcuts.UserRegistration()
                .Register(new
                {
                    UserName = userName,
                    Password,
                    ConfirmPassword = Password
                });
        }
    }
}