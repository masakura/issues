﻿using System;
using System.Linq;
using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class IssueAuthorTest : SignInUseWebDriver
    {
        private IssuePage SubmitIssue()
        {
            var newIssue = Shortcuts.NewIssue();

            return newIssue.Submit(new
            {
                Title = "title",
                Description = "description"
            });
        }

        [Fact]
        public void SubmitAndShow()
        {
            var issue = SubmitIssue();

            Assert.Equal(issue.Author, $"@{TestUser}");
        }

        [Fact]
        public void SubmitAndList()
        {
            SubmitIssue();

            var result = Shortcuts.Issues().Issues.First();
            
            Assert.Equal(result.Author, $"@{TestUser}");
        }

        [Fact]
        public void SubmitAndEdit()
        {
            var result = SubmitIssue().ClickEdit();

            Assert.Equal(result.Author, $"@{TestUser}");
        }

        [Fact]
        public void SubmitAndOtherEdit()
        {
            var issue = SubmitIssue();
            var id = (string) issue.Id;

            var other = Guid.NewGuid().ToString();
            RegisterTestUser(other);
            SignInTestUser(other);

            var result = Shortcuts.Issue(id)
                .ClickEdit()
                .Save(new
                {
                    Title = "edit",
                    Description = "description"
                });

            Assert.Equal($"@{TestUser}", result.Author);
        }
    }
}