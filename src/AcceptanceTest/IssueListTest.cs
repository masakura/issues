﻿using System.Linq;
using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class IssueListTest : UseWebDriver
    {
        public IssueListTest()
        {
            _term = new Term();

            _target = Shortcuts.Issues();
        }

        private readonly IssueListPage _target;
        private readonly Term _term;

        private void Submit(string title, string description)
        {
            Shortcuts.NewIssue().Submit(new {Title = title, Description = description});
        }

        [Fact]
        public void ClickNewIssue()
        {
            Submit("title", "description");

            Shortcuts.Issues().ClickNewIssue();
        }

        [Fact]
        public void GoToIssue()
        {
            Submit("hello1", "description1");
            Submit("hello2", "description2");

            Shortcuts.Issues();

            var result = _target.Issues.Skip(1).First()
                .ClickLink();

            Assert.Equal("hello1", result.Title);
        }

        [Fact]
        public void IssueList()
        {
            var @base = _target.Issues.Count();

            Submit("title1", "description1");
            Submit("title2", "description2");
            Submit("title3", "description3");

            Shortcuts.Issues();

            var result = _target.Issues.ToArray();

            Assert.Equal(@base + 3, result.Length);
            Assert.Equal(new[] {"title3", "title2", "title1"},
                result.Take(3).Select(i => (string) i.Title).ToArray());
            Assert.All(result.Take(3), i => Assert.Equal("less than a minute ago", i.TimeAgo));
            Assert.All(result.Take(3), i => Assert.InRange(i.TimeAgo.Timestamp, _term.Low, _term.High));
        }

        [Fact]
        public void ReverseOrderByRegistration()
        {
            Submit("title1", "description1");
            Submit("title2", "description2");
            Submit("title3", "description3");

            Shortcuts.Issues();

            var result = _target.Issues.Take(3);

            Assert.Equal(new[]
            {
                "title3",
                "title2",
                "title1"
            }, result.Select(row => (string) row.Title).ToArray());
        }
    }
}