﻿namespace AcceptanceTest
{
    public abstract class SignInUseWebDriver : RegistrationUseWebDriver
    {
        protected SignInUseWebDriver()
        {
            SignInTestUser();
        }

        private void SignInTestUser()
        {
            SignInTestUser(TestUser);
        }

        protected void SignInTestUser(string userName)
        {
            Shortcuts.UserSignIn()
                .SignIn(new
                {
                    UserName = userName,
                    Password
                });
        }
    }
}