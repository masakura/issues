﻿using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class NewIssueValidationTest : UseWebDriver
    {
        private readonly CreationIssuePage _target;

        public NewIssueValidationTest()
        {
            _target = Shortcuts.NewIssue();
        }

        [Fact]
        public void EmptyTitleError()
        {
            _target.Submit(new {Title = string.Empty, Description = "Description1"});

            Assert.True(_target.Title.HasError);
            Assert.Equal(RequiredError("Title"), _target.Title.Errors);
        }
        
        [Fact]
        public void LongTitleError()
        {
            _target.Submit(new {Title = LongText(129), Description = "Description1"});

            Assert.True(_target.Title.HasError);
            Assert.Equal(MaxLengthError("Title", 128), _target.Title.Errors);
        }

        [Fact]
        public void LongTitleSuccess()
        {
            var text = LongText(128);

            var createdPage = _target.Submit(new {Title = text, Description = "Description1"});

            Assert.Equal(text, createdPage.Title);
        }
        
        [Fact]
        public void LongDescriptionError()
        {
            _target.Submit(new {Title = "Title1", Description = LongText(5001)});

            Assert.True(_target.Description.HasError);
            Assert.Equal(MaxLengthError("Description", 5000), _target.Description.Errors);
        }

        [Fact]
        public void LongDescriptionSuccess()
        {
            var text = LongText(5000);

            var createdPage = _target.Submit(new {Title = "Title1", Description = text});

            Assert.Equal(text, createdPage.Description);
        }

        private static string LongText(int size)
        {
            return string.Join(string.Empty, new int[size]);
        }

        private static string RequiredError(string name)
        {
            return $"The {name} field is required.";
        }
        
        private static string MaxLengthError(string name, int maximum)
        {
            return $"The field {name} must be a string with a maximum length of {maximum}.";
        }
    }
}