﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class WebControl : WebStatic
    {
        public WebControl(ISearchContext context, string cssSelectorToFind) : base(context, cssSelectorToFind)
        {
        }

        public void NewInput(string text)
        {
            Target.NewInput(text, Context as IWebDriver);
        }

        public string Raw => Target.GetAttribute("innerHTML");

        public bool HasError => Target.HasClass("input-validation-error");

        public string Errors
        {
            get
            {
                var name = Target.GetAttribute("name");
                var error = Context.FindElement(By.CssSelector($"[data-valmsg-for=\"{name}\"]"));
                return error.Text;
            }
        }
    }
}