﻿using System.Linq;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class UserProfilePage : PageBase
    {
        public UserProfilePage(IWebDriver driver) : base(driver)
        {
        }

        public WebStatic UserName => FindControl(".user .username");
        public bool NotFound => !Finds(".user .username").Any();
    }
}