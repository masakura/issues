﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class UserRegistrationPage : PageBase
    {
        public UserRegistrationPage(IWebDriver driver) : base(driver)
        {
        }

        public WebControl UserName => FindControl(".user-edit .username");
        public WebControl Password => FindControl(".user-edit .password");
        public WebControl ConfirmPassword => FindControl(".user-edit .confirm");
        public bool AlreadyTransition => Driver.Url.EndsWith("/Users/Create");

        public void Register(dynamic user)
        {
            UserName.NewInput((string) user.UserName);
            Password.NewInput((string) user.Password);
            ConfirmPassword.NewInput((string) user.ConfirmPassword);

            Find(".user-edit .register").Click();
        }

        public UserSignInPage SwitchSignIn()
        {
            Find(".switch .signin").Click();

            return new UserSignInPage(Driver);
        }
    }
}