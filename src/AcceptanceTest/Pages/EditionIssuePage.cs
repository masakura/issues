﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class EditionIssuePage : ModifilableIssuePage
    {
        public EditionIssuePage(IWebDriver driver) : base(driver)
        {
        }

        public WebStatic Author => FindControl(".issue-edit .author");

        public IssuePage Save(dynamic issue)
        {
            return Post(issue, Find(".issue-edit .save"));
        }

        public UserProfilePage ClickAuthor()
        {
            Author.Click();
            
            return new UserProfilePage(Driver);
        }
    }
}