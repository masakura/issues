﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class HomePage : PageBase
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
        }
    }
}