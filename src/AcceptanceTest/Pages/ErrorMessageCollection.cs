﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class ErrorMessageCollection : IEnumerable<ErrorMessage>
    {
        private readonly IEnumerable<ErrorMessage> _messages;

        private ErrorMessageCollection(IEnumerable<ErrorMessage> messages)
        {
            _messages = messages.ToArray();
        }

        public IEnumerator<ErrorMessage> GetEnumerator()
        {
            return _messages.GetEnumerator();
        }

        public override string ToString()
        {
            var messages = _messages.Select(m => (string) m);

            return string.Join("\n", messages);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static ErrorMessageCollection From(IWebElement context)
        {
            var errors = context.Finds("li")
                .Select(ErrorMessage.From);

            return new ErrorMessageCollection(errors);
        }
    }
}