﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class IssueListPage : PageBase
    {
        public IssueListPage(IWebDriver driver) : base(driver)
        {
        }

        public IEnumerable<IssueRowPage> Issues => IssueRowPage.Rows(Driver, Find(".issue-list"));

        public CreationIssuePage ClickNewIssue()
        {
            Find(".new-issue").Click();
            
            return new CreationIssuePage(Driver);
        }
    }
}