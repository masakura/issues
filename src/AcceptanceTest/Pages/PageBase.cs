﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public class PageBase
    {
        protected readonly IWebDriver Driver;

        protected PageBase(IWebDriver driver)
        {
            Driver = driver;
        }

        public string PageTitle => Find(".page-title").Text;

        protected IWebElement Find(string cssSelectorToFind)
        {
            return Driver.Find(cssSelectorToFind);
        }

        protected IEnumerable<IWebElement> Finds(string cssSelectorToFind)
        {
            return Driver.Finds(cssSelectorToFind);
        }

        protected WebControl FindControl(string cssSelectorToFind)
        {
            return new WebControl(Driver, cssSelectorToFind);
        }

        protected bool Exists(string cssSelectorToFind)
        {
            return Finds(cssSelectorToFind).Any();
        }
    }
}