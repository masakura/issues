﻿using System;
using System.Net;

namespace AcceptanceTest.Selenium
{
    public sealed class PassThroughProxy : IWebProxy
    {
        public Uri GetProxy(Uri destination)
        {
            return destination;
        }

        public bool IsBypassed(Uri host)
        {
            return false;
        }

        public ICredentials Credentials { get; set; }
    }
}