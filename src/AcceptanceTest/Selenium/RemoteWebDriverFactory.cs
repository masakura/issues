﻿using System;
using System.Net;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace AcceptanceTest.Selenium
{
    public sealed class RemoteWebDriverFactory : WebDriverFactory
    {
        private readonly Uri _remote;

        public RemoteWebDriverFactory(Uri remote)
        {
            _remote = remote;
        }

        protected override IWebDriver CreateFirefoxDriver()
        {
            return CreateRemoteWebDriver(new FirefoxOptions());
        }

        private RemoteWebDriver CreateRemoteWebDriver(DriverOptions options)
        {
            return new PassThroughRemoteWebDriver(_remote, options.ToCapabilities());
        }

        protected override IWebDriver CreateChromeDriver()
        {
            return CreateRemoteWebDriver(new ChromeOptions());
        }

        private sealed class PassThroughRemoteWebDriver : RemoteWebDriver
        {
            public PassThroughRemoteWebDriver(Uri remoteAddress, ICapabilities desiredCapabilities) : base(
                CreateHttpCommandExecutor(remoteAddress), desiredCapabilities)
            {
            }

            private static PassThroughHttpCommandExecutor CreateHttpCommandExecutor(Uri remoteAddress)
            {
                return new PassThroughHttpCommandExecutor(remoteAddress, DefaultCommandTimeout, new PassThroughProxy());
            }
        }

        private sealed class PassThroughHttpCommandExecutor : HttpCommandExecutor
        {
            private readonly IWebProxy _proxy;

            public PassThroughHttpCommandExecutor(Uri addressOfRemoteServer, TimeSpan timeout, IWebProxy proxy) : base(
                addressOfRemoteServer, timeout)
            {
                _proxy = proxy;
            }

            protected override HttpWebRequest CreateWebRequest(Uri remote)
            {
                var request = base.CreateWebRequest(remote);
                if (_proxy != null) request.Proxy = _proxy;
                return request;
            }
        }
    }
}