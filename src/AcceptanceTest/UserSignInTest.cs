﻿using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class UserSignInTest : RegistrationUseWebDriver
    {
        private readonly UserSignInPage _target;

        public UserSignInTest()
        {
            _target = Shortcuts.UserSignIn();
        }

        [Fact]
        public void Initial()
        {
            Assert.False(Navigator.IsSignIn);
            Assert.True(Navigator.IsSignInRegisterShown);
        }

        [Fact]
        public void SignIn()
        {
            _target.SignIn(new
            {
                UserName = TestUser,
                Password
            });

            var result = Navigator.SignInUser;

            Assert.Equal($"@{TestUser}", result);
            Assert.False(Navigator.IsSignInRegisterShown);
        }

        [Fact]
        public void SignInMissPassword()
        {
            _target.SignIn(new
            {
                UserName = TestUser,
                Password = "miss"
            });

            Assert.Equal("ユーザー名またはパスワードが異なります。", $"{_target.Errors}");
            Assert.False(Navigator.IsSignIn);
            Assert.True(Navigator.IsSignInRegisterShown);
        }

        [Fact]
        public void SignInMissUserName()
        {
            _target.SignIn(new
            {
                UserName = "miss",
                Password
            });

            Assert.Equal("ユーザー名またはパスワードが異なります。", $"{_target.Errors}");
            Assert.False(Navigator.IsSignIn);
            Assert.True(Navigator.IsSignInRegisterShown);
        }

        [Fact]
        public void SignOut()
        {
            _target.SignIn(new
            {
                UserName = TestUser,
                Password
            });

            Navigator.SignOut();

            Assert.False(Navigator.IsSignIn);
            Assert.True(Navigator.IsSignInRegisterShown);
        }
    }
}