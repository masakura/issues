﻿using System;
using AcceptanceTest.Pages;
using AcceptanceTest.Selenium;
using OpenQA.Selenium;

namespace AcceptanceTest
{
    public abstract class WebDriverFactory
    {
        public static IWebDriver GetWebDriver()
        {
            var factory = GetFactory();
            var driver = factory.CreateDriver(GetBrowserName());
            driver.GoToPage("");
//            driver.Navigate().GoToUrl(Environment.GetEnvironmentVariable("TARGET_URL") ?? "http://localhost:5000");
            return driver;
        }

        private static WebDriverFactory GetFactory()
        {
            var remote = GetRemote();
            return remote != null
                ? (WebDriverFactory) new RemoteWebDriverFactory(remote)
                : new LocalWebDriverFactory();
        }

        private IWebDriver CreateDriver(string browserName)
        {
            switch (browserName)
            {
                case "chrome": return CreateChromeDriver();
                case "firefox": return CreateFirefoxDriver();
                default: throw new InvalidOperationException();
            }
        }

        private static Uri GetRemote()
        {
            var value = Environment.GetEnvironmentVariable("SELENIUM_HUB_URL");

            return string.IsNullOrWhiteSpace(value) ? null : new Uri(value);
        }

        private static string GetBrowserName()
        {
            var value = Environment.GetEnvironmentVariable("SELENIUM_BROWSER_NAME");

            return string.IsNullOrWhiteSpace(value) ? "chrome" : value;
        }

        protected abstract IWebDriver CreateFirefoxDriver();

        protected abstract IWebDriver CreateChromeDriver();
    }
}