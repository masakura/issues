﻿using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class AnonymouseUserProfileTest : UseWebDriver
    {
        private IssuePage SubmitIssue()
        {
            var newIssue = Shortcuts.NewIssue();

            return newIssue.Submit(new
            {
                Title = "title",
                Description = "description"
            });
        }

        [Fact]
        public void IssueWrittenByAnnonymouse()
        {
            var result = SubmitIssue();
            var url = result.Url;

            result.ClickAuthor();
            
            Assert.Equal(url, result.Url);
        }
    }
}