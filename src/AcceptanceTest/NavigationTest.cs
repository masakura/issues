﻿using Xunit;

namespace AcceptanceTest
{
    public class NavigationTest : UseWebDriver
    {
        [Fact]
        public void GoToHome()
        {
            var result = Navigator.GoToHome();

            Assert.Equal("Home", result.PageTitle);
        }

        [Fact]
        public void GoToIssues()
        {
            var result = Navigator.GoToIssues();

            Assert.Equal("Issues", result.PageTitle);
        }

        [Fact]
        public void GoToUserRegistrationOrSignIn()
        {
            var result = Navigator.GoToUserRegistrationOrSignIn();
            
            Assert.Equal("Sign in", result.PageTitle);
        }

        [Fact]
        public void NavigationLeftMenuItems()
        {
            var result = Navigator.GetLeftMenuItems();

            Assert.Equal(new[] {"Home", "Issues"}, result);
        }

        [Fact]
        public void NavigationRightMenuItems()
        {
            var result = Navigator.GetRightMenuItems();

            Assert.Equal(new[] {"Sign in / Register"}, result);
        }
    }
}