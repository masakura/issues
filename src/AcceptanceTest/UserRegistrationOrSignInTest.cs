﻿using Xunit;

namespace AcceptanceTest
{
    public sealed class UserRegistrationOrSignInTest : UseWebDriver
    {
        [Fact]
        public void SwitchSignIn()
        {
            var result = Shortcuts
                .UserRegistration()
                .SwitchSignIn();

            Assert.Equal("Sign in", result.PageTitle);
        }

        [Fact]
        public void SwitchUserRegistration()
        {
            var result = Shortcuts
                .UserSignIn()
                .SwitchUserRegistration();

            Assert.Equal("User registration", result.PageTitle);
        }
    }
}