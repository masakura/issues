﻿using System;
using System.Linq;
using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class UserProfileTest : SignInUseWebDriver
    {
        public UserProfileTest()
        {
            _displayedTestUser = $"@{TestUser}";
        }

        private readonly string _displayedTestUser;

        private IssuePage SubmitIssue()
        {
            var newIssue = Shortcuts.NewIssue();

            return newIssue.Submit(new
            {
                Title = "title",
                Description = "description"
            });
        }

        [Fact]
        public void Anonymouse()
        {
            var result = Shortcuts.UserProfile("anonymouse");

            Assert.True(result.NotFound);
        }

        [Fact]
        public void EditIssue()
        {
            var result = SubmitIssue()
                .ClickEdit()
                .ClickAuthor();

            Assert.Equal("User profile", result.PageTitle);
            Assert.Equal(_displayedTestUser, result.UserName);
        }

        [Fact]
        public void GoToUserProfile()
        {
            var result = Navigator.GoToProfile();

            Assert.Equal("My profile", result.PageTitle);
        }

        [Fact]
        public void Issues()
        {
            SubmitIssue();

            var result = Shortcuts.Issues().Issues.First().ClickAuthor();

            Assert.Equal("User profile", result.PageTitle);
            Assert.Equal(_displayedTestUser, result.UserName);
        }

        [Fact]
        public void MyProfile()
        {
            var result = Shortcuts.MyProfile();

            Assert.Equal("My profile", result.PageTitle);
            Assert.Equal(_displayedTestUser, result.UserName);
        }

        [Fact]
        public void NotRegistered()
        {
            var result = Shortcuts.UserProfile(Guid.NewGuid().ToString());

            Assert.True(result.NotFound);
        }

        [Fact]
        public void ShowIssue()
        {
            var result = SubmitIssue().ClickAuthor();

            Assert.Equal("User profile", result.PageTitle);
            Assert.Equal(_displayedTestUser, result.UserName);
        }

        [Fact]
        public void Unknown()
        {
            var result = Shortcuts.UserProfile("unknown");

            Assert.True(result.NotFound);
        }

        [Fact]
        public void UserProfile()
        {
            var result = Shortcuts.UserProfile(TestUser);

            Assert.Equal("User profile", result.PageTitle);
            Assert.Equal(_displayedTestUser, result.UserName);
        }
    }
}