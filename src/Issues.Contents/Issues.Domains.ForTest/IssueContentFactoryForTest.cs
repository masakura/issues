﻿using System.Threading.Tasks;
// ReSharper disable MemberCanBePrivate.Global

namespace Issues.Domains.ForTest
{
    public static class IssueContentFactoryForTest
    {
        public static IssueContent NewContent(string title, string description)
        {
            return new IssueContent((IssueTitle) title, (IssueDescription) description);
        }

        public static IssueContent NewContent(int id, string title = null, string description = null)
        {
            var t = title ?? $"title{id}";
            var d = description ?? $"description{id}";

            return NewContent(t, d);
        }

        public static IssueContent NewContent(IssueId id, string title = null, string description = null)
        {
            return NewContent((int) id, title, description);
        }

        public static Task<IssueContent> NewContentAsync(IssueId id, string title = null, string description = null)
        {
            return Task.FromResult(NewContent(id, title, description));
        }
    }
}