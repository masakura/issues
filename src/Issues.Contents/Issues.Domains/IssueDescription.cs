﻿using static Issues.ValueObjectUtil;

namespace Issues.Domains
{
    public struct IssueDescription
    {
        private readonly string _value;

        public IssueDescription(string value)
        {
            _value = Normalize(value);
        }

        public override string ToString()
        {
            return _value;
        }

        public static explicit operator IssueDescription(string description)
        {
            return new IssueDescription(description);
        }

        public static explicit operator string(IssueDescription description)
        {
            return description._value;
        }
    }
}