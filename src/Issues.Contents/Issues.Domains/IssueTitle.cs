﻿using static Issues.ValueObjectUtil;

namespace Issues.Domains
{
    public struct IssueTitle
    {
        private readonly string _value;

        public IssueTitle(string value)
        {
            _value = Normalize(value);
        }

        public override string ToString()
        {
            return _value;
        }

        public static explicit operator string(IssueTitle title)
        {
            return title._value;
        }

        public static explicit operator IssueTitle(string title)
        {
            return new IssueTitle(title);
        }
    }
}