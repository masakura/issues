﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueContentRepository
    {
        Task<IssueContent> FindAsync(IssueId id);
        Task CreateAsync(IssueId id, IssueContent content);
        Task SaveAsync(IssueId id, IssueContent content);
    }
}