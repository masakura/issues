﻿using Issues.Domains;
using Xunit;
using static Issues.Domains.ForTest.IssueContentFactoryForTest;

namespace Issues
{
    public sealed class IssueContentViewModelTest
    {
        [Fact]
        public void CastToIssueContent()
        {
            var viewModel = new IssueContentViewModel
            {
                Title = "hello",
                Description = "my name is"
            };

            var result = (IssueContent) viewModel;
            
            Assert.Equal("hello", (string) result.Title);
            Assert.Equal("my name is", (string) result.Description);
        }

        [Fact]
        public void CastFormIssueContent()
        {
            var result = (IssueContentViewModel) NewContent("hello", "my name is");
            
            Assert.Equal("hello", result.Title);
            Assert.Equal("my name is", result.Description);
        }
    }
}