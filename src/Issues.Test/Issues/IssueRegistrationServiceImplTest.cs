﻿using System.Threading.Tasks;
using Issues.Domains;
using Moq;
using Xunit;
using static Issues.Domains.MoqItIsUtility;

namespace Issues
{
    public class IssueRegistrationServiceImplTest
    {
        public IssueRegistrationServiceImplTest()
        {
            _mock = new Mock<IIssueRegistry>();

            _target = new IssueRegistrationServiceImpl(_mock.Object);
        }

        private readonly IssueRegistrationServiceImpl _target;
        private readonly Mock<IIssueRegistry> _mock;

        [Fact]
        public async Task SaveAsync()
        {
            _mock.Setup(x => x.SaveAsync(IsId(15), IsContent("modified t", "modified d")))
                .Returns(Task.CompletedTask);

            await _target.SaveAsync((IssueId) 15, new IssueContentViewModel {Title = "modified t", Description = "modified d"});

            _mock.VerifyAll();
        }

        [Fact]
        public async Task SubmitAsync()
        {
            _mock.Setup(x => x.CreateAsync(IsContent("Bye", "Bye bye!")))
                .Returns(() => Task.FromResult(new IssueId(15)));

            var id = await _target.SubmitAsync(new IssueContentViewModel {Title = "Bye", Description = "Bye bye!"});

            Assert.Equal((IssueId) 15, id);

            _mock.VerifyAll();
        }
    }
}