﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;
using static Issues.Domains.ForTest.AuthoredIssueBasisFactoryForTest;
using static Issues.Domains.ForTest.IssueContentFactoryForTest;

namespace Issues.Domains
{
    public sealed class IssueLibraryImplTest
    {
        public IssueLibraryImplTest()
        {
            _mockBasis = new Mock<IAuthoredIssueBasisLibrary>();
            _mockContents = new Mock<IIssueContentRepository>();

            _target = new IssueLibraryImpl(_mockBasis.Object, _mockContents.Object);
        }

        private readonly IssueLibraryImpl _target;
        private readonly Mock<IAuthoredIssueBasisLibrary> _mockBasis;
        private readonly Mock<IIssueContentRepository> _mockContents;

        [Fact]
        public async Task AllAsync()
        {
            _mockBasis.Setup(x => x.AllAsync())
                .Returns(NewAuthoredBasisesAsync(3));
            _mockContents.Setup(x => x.FindAsync(It.IsAny<IssueId>()))
                .Returns<IssueId>(id => NewContentAsync(id));

            var result = await _target.AllAsync();

            Assert.Equal(3, result.Count());
            Assert.Equal("title3", (string) result.ElementAt(0).Content.Title);
            Assert.Equal("description3", (string) result.ElementAt(0).Content.Description);
            Assert.Equal("title2", (string) result.ElementAt(1).Content.Title);
            Assert.Equal("description2", (string) result.ElementAt(1).Content.Description);
            Assert.Equal("title1", (string) result.ElementAt(2).Content.Title);
            Assert.Equal("description1", (string) result.ElementAt(2).Content.Description);
        }

        [Fact]
        public async Task AllAsyncExcludeBrokens()
        {
            _mockBasis.Setup(x => x.AllAsync())
                .Returns(NewAuthoredBasisesAsync(3));
            _mockContents.Setup(x => x.FindAsync(It.IsAny<IssueId>()))
                .Returns<IssueId>(id => (int) id == 2 ? Task.FromResult(IssueContent.NotFound) : NewContentAsync(id));

            var result = await _target.AllAsync();

            Assert.Equal(2, result.Count());

            Assert.True(result.Any(issue => (string) issue.Content.Title == "title1"));
            Assert.True(result.Any(issue => (string) issue.Content.Title == "title3"));
        }

        [Fact]
        public async Task FindAsync()
        {
            var id = (IssueId) 15;

            _mockBasis.Setup(x => x.FindAsync(It.IsAny<IssueId>()))
                .Returns<IssueId>(NewAuthoredBasisAsync);
            _mockContents.Setup(x => x.FindAsync(It.IsAny<IssueId>()))
                .Returns<IssueId>(issueId => NewContentAsync(issueId));

            var result = await _target.FindAsync(id);

            Assert.Equal(id, result.Id);
            Assert.Equal("title15", (string) result.Content.Title);
            Assert.Equal("description15", (string) result.Content.Description);
            Assert.Equal(DateTime.Parse("2015/1/15"), (DateTime) result.RegistrationAt);
            Assert.Equal("user15", (string) result.Author.UserName);
        }
    }
}