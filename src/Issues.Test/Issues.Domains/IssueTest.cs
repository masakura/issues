﻿using Xunit;
using static Issues.Domains.ForTest.IssueFactoryForTest;

namespace Issues.Domains
{
    public sealed class IssueTest
    {
        [Fact]
        public void GreaterThan()
        {
            var left = NewIssue(1, "2017/10/10");
            var right = NewIssue(2, "2017/10/11");

            var result = left.CompareTo(right);
            
            Assert.True(result > 0);
        }

        [Fact]
        public void LessThan()
        {
            var left = NewIssue(1, "2017/10/11");
            var right = NewIssue(2, "2017/10/10");

            var result = left.CompareTo(right);
            
            Assert.True(result < 0);
        }

        [Fact]
        public void Equal()
        {
            var left = NewIssue(1, "2017/10/10");
            var right = NewIssue(2, "2017/10/10");

            var result = left.CompareTo(right);

            Assert.Equal(0, result);
        }
    }
}