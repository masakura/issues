﻿using System.Threading.Tasks;
using Moq;
using Xunit;
using static Issues.Domains.ForTest.IssueContentFactoryForTest;
using static Issues.Domains.ForTest.IssueIdFactoryForTest;
using static Issues.Domains.MoqItIsUtility;

namespace Issues.Domains
{
    public sealed class IssueRegistryImplTest
    {
        public IssueRegistryImplTest()
        {
            _mockBasis = new Mock<IAuthoredIssueBasisRegistry>();
            _mockContents = new Mock<IIssueContentRepository>();

            _target = new IssueRegistryImpl(_mockBasis.Object,
                _mockContents.Object);
        }

        private readonly IssueRegistryImpl _target;
        private readonly Mock<IAuthoredIssueBasisRegistry> _mockBasis;
        private readonly Mock<IIssueContentRepository> _mockContents;

        [Fact]
        public async Task CreateAsync()
        {
            _mockBasis.Setup(x => x.CreateAsync())
                .Returns(() => NewIdAsync(15));
            _mockContents.Setup(x => x.CreateAsync(IsId(15), IsContent("hello", "my name is")))
                .Returns(Task.CompletedTask);

            var id = await _target.CreateAsync(NewContent("hello", "my name is"));

            Assert.Equal(15, (int) id);

            _mockBasis.VerifyAll();
            _mockContents.VerifyAll();
        }

        [Fact]
        public async Task SaveAsync()
        {
            _mockContents.Setup(x => x.SaveAsync(IsId(15), IsContent("modified t", "modified d")))
                .Returns(Task.CompletedTask);

            await _target.SaveAsync(NewId(15), NewContent("modified t", "modified d"));

            _mockContents.VerifyAll();
        }
    }
}