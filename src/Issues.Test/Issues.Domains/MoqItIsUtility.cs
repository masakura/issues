﻿using Moq;
// ReSharper disable MemberCanBePrivate.Global

namespace Issues.Domains
{
    public static class MoqItIsUtility
    {
        public static IssueContent IsContent(string title, string description)
        {
            return It.Is<IssueContent>(content =>
                (string) content.Title == title && (string) content.Description == description);
        }

        public static IssueId IsId(int id)
        {
            return It.Is<IssueId>(i => (int) i == id);
        }
    }
}