﻿using Issues.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class InfrastructureServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddIssueDomains(this IServiceCollection services)
        {
            services.AddTransient<ITimestamp, TimestampImpl>();

            return services;
        }
    }
}