﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Issues
{
    public struct OccurrenceDateTime : IComparable<OccurrenceDateTime>
    {
        private readonly DateTime _value;

        public OccurrenceDateTime(DateTime value)
        {
            _value = value;
        }

        public static explicit operator DateTime(OccurrenceDateTime occurrence)
        {
            return occurrence._value;
        }

        public static explicit operator OccurrenceDateTime(DateTime dateTime)
        {
            return new OccurrenceDateTime(dateTime);
        }

        private sealed class ValueRelationalComparer : Comparer<OccurrenceDateTime>
        {
            public override int Compare(OccurrenceDateTime x, OccurrenceDateTime y)
            {
                // ReSharper disable once ImpureMethodCallOnReadonlyValueField
                return x._value.CompareTo(y._value);
            }
        }

        private static Comparer<OccurrenceDateTime> DefaultCOmparer { get; } = new ValueRelationalComparer();
        // ReSharper disable once ImpureMethodCallOnReadonlyValueField
        // ReSharper disable once UnusedMember.Global
        public string Timestamp => _value.ToString("s", CultureInfo.InvariantCulture);

        public int CompareTo(OccurrenceDateTime other)
        {
            return DefaultCOmparer.Compare(this, other);
        }

        public override string ToString()
        {
            // ReSharper disable once ImpureMethodCallOnReadonlyValueField
            return _value.ToLocalTime().ToString("U", CultureInfo.InvariantCulture);
        }
    }
}