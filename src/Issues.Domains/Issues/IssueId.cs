﻿namespace Issues
{
    public struct IssueId
    {
        private readonly int _value;

        public IssueId(int value)
        {
            _value = value;
        }

        public static explicit operator int(IssueId id)
        {
            return id._value;
        }

        public static explicit operator IssueId(int id)
        {
            return new IssueId(id);
        }

        public static bool operator ==(IssueId left, IssueId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IssueId left, IssueId right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"#{_value}";
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public bool Equals(IssueId other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is IssueId id && Equals(id);
        }

        public override int GetHashCode()
        {
            return _value;
        }
    }
}