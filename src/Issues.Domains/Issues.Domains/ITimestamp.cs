﻿namespace Issues.Domains
{
    public interface ITimestamp
    {
        OccurrenceDateTime Now();
    }
}