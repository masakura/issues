﻿using System.Threading.Tasks;
using Issues.Users;
using Xunit;

namespace Integrations
{
    public sealed class UserSignInTest : UserRegistrationTestBase
    {
        public UserSignInTest()
        {
            _target = GetService<IUserSignInService>();
        }

        private readonly IUserSignInService _target;

        [Fact]
        public async Task SignInAsync()
        {
            var errors = await _target.SignInAsync(TestUser, TestPassword);

            Assert.False(errors.HasErrors);
        }

        [Fact]
        public new async Task SignOutAsync()
        {
            await _target.SignInAsync(TestUser, TestPassword);
            await _target.SignOutAsync();

            var user = await _target.GetCurrentUserAsync();
            
            Assert.False(user.IsAuthenticated);
            Assert.Equal(UserName.Anonymouse, user.UserName);
        }

        [Fact]
        public async Task SignInAsyncMissPassword()
        {
            var errors = await _target.SignInAsync(TestUser, (Password) "miss");

            Assert.True(errors.HasErrors);
            Assert.Equal("ユーザー名またはパスワードが異なります。", $"{errors}");
        }

        [Fact]
        public async Task SignInAsyncMissUserName()
        {
            var errors = await _target.SignInAsync((UserName) "miss", TestPassword);

            Assert.True(errors.HasErrors);
            Assert.Equal("ユーザー名またはパスワードが異なります。", $"{errors}");
        }

        [Fact]
        public async Task GetCurrentUserAsync()
        {
            await _target.SignInAsync(TestUser, TestPassword);
            var result = await _target.GetCurrentUserAsync();
            
            Assert.True(result.IsAuthenticated);
            Assert.Equal(TestUser, result.UserName);
        }

        [Fact]
        public async Task GetCurrentUserAsyncNotSignIn()
        {
            var result = await _target.GetCurrentUserAsync();
            
            Assert.False(result.IsAuthenticated);
            Assert.Equal(UserName.Anonymouse, result.UserName);
        }
    }
}