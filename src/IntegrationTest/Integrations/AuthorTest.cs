﻿using System.Linq;
using System.Threading.Tasks;
using Issues;
using Issues.Users;
using Xunit;

namespace Integrations
{
    public sealed class AuthorTest : UserRegistrationTestBase
    {
        public AuthorTest()
        {
            _registration = GetService<IIssueRegistrationService>();
            _issues = GetService<IIssueLibraryService>();

            SignInAsync(TestUser).Wait();
        }

        private readonly IIssueRegistrationService _registration;
        private readonly IIssueLibraryService _issues;

        private async Task<IssueId> SubmitAsync()
        {
            var issueId = await _registration.SubmitAsync(new IssueContentViewModel
            {
                Title = "title",
                Description = "description"
            });
            return issueId;
        }

        private async Task SaveAsync(IssueId issueId)
        {
            await _registration.SaveAsync(issueId, new IssueContentViewModel
            {
                Title = "new",
                Description = "new"
            });
        }

        [Fact]
        public async Task AllAsync()
        {
            await SignInAsync(TestUser);
            await SubmitAsync();
            await SubmitAsync();
            await SignInAsync(OtherUser);
            await SubmitAsync();
            await SignOutAsync();
            await SubmitAsync();

            var all = await _issues.AllAsync();

            var result = all.Authors().UserNames().ToArray();

            Assert.Equal(new[]
            {
                UserName.Anonymouse,
                OtherUser,
                TestUser,
                TestUser
            }, result);
        }

        [Fact]
        public async Task BindAsync()
        {
            var issueId = await SubmitAsync();

            var result = await _issues.FindAsync(issueId);

            Assert.Equal(TestUser, result.Author.UserName);
        }

        [Fact]
        public async Task ModifyByOtherIssueIsNotChangeAsync()
        {
            var issueId = await SubmitAsync();
            await SignInAsync(OtherUser);
            await SaveAsync(issueId);

            var result = await _issues.FindAsync(issueId);

            Assert.Equal(TestUser, result.Author.UserName);
        }
    }
}