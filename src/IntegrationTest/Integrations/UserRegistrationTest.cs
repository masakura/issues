﻿using System.Threading.Tasks;
using Issues.Users;
using Xunit;

namespace Integrations
{
    public sealed class UserRegistrationTest : IntegrationTestBase
    {
        public UserRegistrationTest()
        {
            _target = GetService<IUserRegistrationService>();
        }

        private readonly IUserRegistrationService _target;

        private struct PasswordSample
        {
            public Password Text { get; }
            public string Error { get; }

            public PasswordSample(string text, string error)
            {
                Text = (Password) text;
                Error = error;
            }
        }

        private sealed class PasswordSampleCollection
        {
            public PasswordSample NonAlphanumeric { get; } = new PasswordSample("abcdEFGH0",
                "Passwords must have at least one non alphanumeric character.");

            public PasswordSample Digit { get; } =
                new PasswordSample("abcdEFGH_", "Passwords must have at least one digit ('0'-'9').");

            public PasswordSample Lowercase { get; } = new PasswordSample("ABCDEFGH_0",
                "Passwords must have at least one lowercase ('a'-'z').");

            public PasswordSample Uppercase { get; } = new PasswordSample("abcdefgh_0",
                "Passwords must have at least one uppercase ('A'-'Z').");

            public PasswordSample Empty { get; } =
                new PasswordSample(string.Empty, "Passwords must be at least 6 characters.");

            public PasswordSample Short { get; } =
                new PasswordSample("Aa_0a", "Passwords must be at least 6 characters.");

            public PasswordSample Valid { get; } = new PasswordSample("Aa_0123456", "");
        }

        private static PasswordSampleCollection Passwords { get; } = new PasswordSampleCollection();

        private async Task<ErrorMessageCollection> TargetValidateUserNameAsync(string userName)
        {
            return await _target.ValidateUserNameAsync((UserName) userName);
        }

        private async Task<ErrorMessageCollection> TargetRegisterAsync(string userName, PasswordSample password)
        {
            return await _target.RegisterAsync((UserName) userName, password.Text);
        }

        private async Task<ErrorMessageCollection> TargetValidatePasswordAsync(PasswordSample password)
        {
            return await _target.ValidatePasswordAsync(password.Text);
        }

        [Fact]
        public async Task RegisterAsync()
        {
            var result = await TargetRegisterAsync("foo@example.com", Passwords.Valid);

            Assert.False(result.HasErrors);
        }

        [Fact]
        public async Task RegisterAsyncDuplicate()
        {
            await TargetRegisterAsync("foo@example.com", Passwords.Valid);
            var result = await TargetRegisterAsync("foo@example.com", Passwords.Valid);

            Assert.True(result.HasErrors);
            Assert.Equal("User name 'foo@example.com' is already taken.", $"{result}");
        }

        [Fact]
        public async Task RegisterAsyncPasswordTooSimple()
        {
            var result = await TargetRegisterAsync("foo@example.com", Passwords.NonAlphanumeric);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.NonAlphanumeric.Error, $"{result}");
        }

        [Fact]
        public async Task ValidatePasswordAsync()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Valid);

            Assert.False(result.HasErrors);
        }

        [Fact]
        public async Task ValidatePasswordAsyncDigit()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Digit);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.Digit.Error, $"{result}");
        }

        [Fact]
        public async Task ValidatePasswordAsyncEmpty()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Empty);

            Assert.True(result.HasErrors);
            Assert.Contains((ErrorMessage) Passwords.Empty.Error, result);
        }

        [Fact]
        public async Task ValidatePasswordAsyncLowercase()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Lowercase);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.Lowercase.Error, $"{result}");
        }

        [Fact]
        public async Task ValidatePasswordAsyncNonAlphaNumeric()
        {
            var result = await TargetValidatePasswordAsync(Passwords.NonAlphanumeric);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.NonAlphanumeric.Error, $"{result}");
        }

        [Fact]
        public async Task ValidatePasswordAsyncShort()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Short);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.Short.Error, $"{result}");
        }

        [Fact]
        public async Task ValidatePasswordAsyncUpperCase()
        {
            var result = await TargetValidatePasswordAsync(Passwords.Uppercase);

            Assert.True(result.HasErrors);
            Assert.Equal(Passwords.Uppercase.Error, $"{result}");
        }

        [Fact]
        public async Task ValidateUserNameAsync()
        {
            var result = await TargetValidateUserNameAsync("user1");

            Assert.False(result.HasErrors);
        }

        [Fact]
        public async Task ValidateUserNameAsyncDuplicate()
        {
            await TargetRegisterAsync("user1", Passwords.Valid);
            var result = await TargetValidateUserNameAsync("user1");

            Assert.True(result.HasErrors);
            Assert.Equal("User name 'user1' is already taken.", $"{result}");
        }

        [Fact]
        public async Task ValidateUserNameAsyncIncludeWhitespace()
        {
            var result = await TargetValidateUserNameAsync("user 1");

            Assert.True(result.HasErrors);
            Assert.Equal("User name 'user 1' is invalid, can only contain letters or digits.", $"{result}");
        }
    }
}