﻿using System.Linq;
using System.Threading.Tasks;
using Issues;
using Xunit;

namespace Integrations
{
    public sealed class IssueListTest : IntegrationTestBase
    {
        public IssueListTest()
        {
            _library = GetService<IIssueLibraryService>();
            _registry = GetService<IIssueRegistrationService>();
        }

        private readonly IIssueLibraryService _library;
        private readonly IIssueRegistrationService _registry;

        private async Task SubmitAsync(string title, string description)
        {
            await _registry.SubmitAsync(new IssueContentViewModel {Title = title, Description = description});
        }

        [Fact]
        public async Task EmptyWhenFirst()
        {
            var result = await _library.AllAsync();

            Assert.Equal(0, result.Length);
        }

        [Fact]
        public async Task RegisterOne()
        {
            await SubmitAsync("title1", "description1");

            var result = await _library.AllAsync();

            Assert.Equal(1, result.Length);
            Assert.Equal("title1", result.ElementAt(0).Content.Title);
            Assert.Equal(Offset(1), result.ElementAt(0).RegistrationAt);
        }

        [Fact]
        public async Task RegisterThree()
        {
            await SubmitAsync("title1", "description1");
            await SubmitAsync("title2", "description2");
            await SubmitAsync("title3", "description3");

            var result = await _library.AllAsync();

            Assert.Equal(3, result.Length);
            Assert.Equal(new[] {"title3", "title2", "title1"},
                result.Select(i => i.Content.Title).ToArray());
            Assert.Equal(new[] {Offset(3), Offset(2), Offset(1)},
                result.Select(i => i.RegistrationAt).ToArray());
        }
    }
}