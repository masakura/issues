﻿using System.Threading.Tasks;
using Issues.Users;

namespace Integrations
{
    public class UserRegistrationTestBase : IntegrationTestBase
    {
        protected readonly UserName OtherUser = (UserName) "other";
        protected readonly Password TestPassword = (Password) "Hogehoge1_";
        protected readonly UserName TestUser = (UserName) "testuser";

        protected UserRegistrationTestBase()
        {
            RegisterUsersAsync().Wait();
        }

        private async Task RegisterUserAsync(UserName userName, Password password)
        {
            await GetService<IUserRegistrationService>()
                .RegisterAsync(userName, password);
        }

        private async Task RegisterUsersAsync()
        {
            await RegisterUserAsync(TestUser, TestPassword);
            await RegisterUserAsync(OtherUser, TestPassword);
        }

        protected async Task SignInAsync(UserName userName)
        {
            await SignOutAsync();
            await GetService<IUserSignInService>().SignInAsync(userName, TestPassword);
        }

        protected async Task SignOutAsync()
        {
            await GetService<IUserSignInService>().SignOutAsync();
        }
    }
}