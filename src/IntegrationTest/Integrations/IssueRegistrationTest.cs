﻿using System.Threading.Tasks;
using Issues;
using Xunit;

namespace Integrations
{
    public sealed class IssueRegistrationTest : IntegrationTestBase
    {
        private readonly IIssueLibraryService _library;
        private readonly IIssueRegistrationService _registry;

        public IssueRegistrationTest()
        {
            _library = GetService<IIssueLibraryService>();
            _registry = GetService<IIssueRegistrationService>();
        }

        [Fact]
        public async Task SubmitAsync()
        {
            var content = new IssueContentViewModel
            {
                Title = "hello",
                Description = "my name is"
            };

            var submittedId = await _registry.SubmitAsync(content);

            var result = await _library.FindAsync(submittedId);
            
            Assert.Equal(1, (int) result.Id);
            Assert.Equal("hello", result.Content.Title);
            Assert.Equal("my name is", result.Content.Description);
            Assert.Equal(Offset(1), result.RegistrationAt);
        }
    }
}