﻿using System;
using Issues.Authors.Infrastructures;
using Issues.Contents.Infrastructures;
using Issues.Domains;
using Issues.Infrastructures;
using Issues.Users.Domains;
using Issues.Users.Infrastructures;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class InfrastructureServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddInfrastructures(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsBuilder)
        {
            services.AddDbContext<ApplicationDbContext>(optionsBuilder);

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IInfrastructureMigrationService, InfrastructureMigrationServiceImpl>();
            services.AddTransient<IIssueBasisRepository, IssueBasisRepositoryImpl>();
            services.AddTransient<IIssueContentRepository, IssueContentRepositoryImpl>();

            services.AddTransient<IUserRepository, UserRepositoryImpl>();

            services.AddTransient<IAuthorRepository, AuthorRepositoryImpl>();

            return services;
        }
    }
}