﻿using Issues.Infrastructures;
using Microsoft.EntityFrameworkCore;

namespace Issues.Contents.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class InfrastructureMigrationServiceImpl : IInfrastructureMigrationService
    {
        private readonly ApplicationDbContext _db;

        public InfrastructureMigrationServiceImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public void EnsureCreated()
        {
            _db.Database.EnsureCreated();
        }

        public void Migrate()
        {
            _db.Database.Migrate();
        }
    }
}