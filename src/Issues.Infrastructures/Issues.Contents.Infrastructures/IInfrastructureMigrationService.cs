﻿namespace Issues.Contents.Infrastructures
{
    public interface IInfrastructureMigrationService
    {
        void EnsureCreated();
        void Migrate();
    }
}