﻿using System.Collections.Generic;
using System.Linq;
using Issues.Domains;

namespace Issues.Contents.Infrastructures
{
    public static class IssueBasisRowEnumerableExtentions
    {
        public static IssueBasisCollection ToDomainCollection(this IEnumerable<IssueBasisRow> rows)
        {
            return rows
                .Select(row => row.ToDomain())
                .ToCollection();
        }
    }
}