﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Issues.Contents.Infrastructures
{
    public static class DbSetIssueContentRowExtensions
    {
        public static Task<IssueContentRow> FindFromAsync(this DbSet<IssueContentRow> row, IssueId id)
        {
            return row.FindAsync((int) id);
        }
    }
}