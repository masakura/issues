﻿using System;
using System.Threading.Tasks;
using Issues.Domains;
using Issues.Infrastructures;

namespace Issues.Contents.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class IssueBasisRepositoryImpl : IIssueBasisRepository
    {
        private readonly ApplicationDbContext _db;

        public IssueBasisRepositoryImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IssueId> CreateAsync(OccurrenceDateTime registrationAt)
        {
            var row = new IssueBasisRow
            {
                RegistrationAt = (DateTime) registrationAt
            };

            var added = await _db.Issues.AddAsync(row);
            await _db.SaveChangesAsync();

            return (IssueId) added.Entity.Id;
        }

        public async Task<IssueBasis> FindAsync(IssueId id)
        {
            var row = await _db.Issues.FindAsync((int) id);
            return row.ToDomain();
        }

        public Task<IssueBasisCollection> AllAsync()
        {
            return Task.FromResult(_db.Issues.ToDomainCollection());
        }
    }
}