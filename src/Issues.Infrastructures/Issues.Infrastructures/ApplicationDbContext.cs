﻿using Issues.Authors.Infrastructures;
using Issues.Contents.Infrastructures;
using Issues.Users.Infrastructures;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Issues.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public DbSet<IssueBasisRow> Issues { get; set; }

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public DbSet<IssueContentRow> IssueContents { get; set; }
        
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public DbSet<AuthorRow> Authors { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<AuthorRow>()
                .HasKey(c => new {IssueId = c.ResourceId, c.Type});
        }
    }
}