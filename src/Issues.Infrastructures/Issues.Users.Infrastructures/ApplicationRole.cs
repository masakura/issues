﻿using Microsoft.AspNetCore.Identity;

namespace Issues.Users.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationRole : IdentityRole<int>
    {
    }
}