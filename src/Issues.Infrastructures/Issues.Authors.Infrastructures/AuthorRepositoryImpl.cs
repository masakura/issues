﻿using System.Threading.Tasks;
using Issues.Domains;
using Issues.Infrastructures;
using Issues.Users;

namespace Issues.Authors.Infrastructures
{
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class AuthorRepositoryImpl : IAuthorRepository
    {
        private readonly ApplicationDbContext _db;

        public AuthorRepositoryImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<UserId> FindByResourcedAsync(ResourceId resourceId, ResourceType type)
        {
            var author = await FindAsync(resourceId, type);
            return (UserId) (author?.AuthorId ?? 0);
        }

        public async Task BindAsync(ResourceId resourceId, ResourceType type, UserId userId)
        {
            var author = await FindAsync(resourceId, type);

            if (author == null)
            {
                author = new AuthorRow {ResourceId = (int) resourceId, Type = (string) type};
                var result = await _db.Authors.AddAsync(author);

                author = result.Entity;
            }

            author.AuthorId = (int) userId;

            await _db.SaveChangesAsync();
        }

        private async Task<AuthorRow> FindAsync(ResourceId issueId, ResourceType type)
        {
            return await _db.Authors.FindAsync((int) issueId, (string) type);
        }
    }
}