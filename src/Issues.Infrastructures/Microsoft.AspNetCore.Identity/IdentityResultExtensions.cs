﻿using System.Collections.Generic;
using Issues.Users;

namespace Microsoft.AspNetCore.Identity
{
    internal static class IdentityResultExtensions
    {
        public static IEnumerable<ErrorMessage> ToErrorMessages(this IdentityResult result)
        {
            return result.Errors.ToErrorMessages();
        }
    }
}