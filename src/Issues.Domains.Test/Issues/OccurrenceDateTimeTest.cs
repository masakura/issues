﻿using System;
using Issues;
using Xunit;

namespace issues
{
    public sealed class OccurrenceDateTimeTest
    {
        [Fact]
        public void CastFromDateTime()
        {
            var dateTime = DateTime.Parse("2012/10/10 11:00:00");

            var result = (OccurrenceDateTime) dateTime;

            Assert.Equal(new OccurrenceDateTime(dateTime), result);
        }

        [Fact]
        public void CastToDateTime()
        {
            var dateTime = DateTime.Parse("2012/10/10 11:00:00");
            var occurrence = new OccurrenceDateTime(dateTime);

            var result = (DateTime) occurrence;

            Assert.Equal(dateTime, result);
        }

        [Fact]
        public void ConvertToString()
        {
            var dateTime = new DateTime(2017, 1, 2, 3, 4, 5);
            var result = new OccurrenceDateTime(dateTime).ToString();

            Assert.Equal("Monday, 02 January 2017 03:04:05", result);
        }
    }
}