﻿using System.Linq;
using System.Threading.Tasks;
using static Issues.Domains.ForTest.IssueBasisFactoryForTest;
using static Issues.Users.ForTest.UserFactoryForTest;

namespace Issues.Domains.ForTest
{
    public static class AuthoredIssueBasisFactoryForTest
    {
        public static Task<AuthoredIssueBasisCollection> NewAuthoredBasisesAsync(int count)
        {
            return Task.FromResult(NewAuthoredBasises(count));
        }

        public static Task<AuthoredIssueBasis> NewAuthoredBasisAsync(IssueId id)
        {
            return Task.FromResult(NewAuthoredBasis(id));
        }

        private static AuthoredIssueBasis NewAuthoredBasis(IssueId id, string registeredAt = null)
        {
            var basis = NewBasis(id, registeredAt);
            return new AuthoredIssueBasis(basis, NewAuthenticated((int) id));
        }

        private static AuthoredIssueBasis NewAuthoredBasis(int id, string registeredAt = null)
        {
            return NewAuthoredBasis((IssueId) id, registeredAt);
        }

        private static AuthoredIssueBasisCollection NewAuthoredBasises(int count)
        {
            return Enumerable.Range(1, count)
                .Select(id => NewAuthoredBasis(id))
                .ToCollection();
        }
    }
}