﻿using System.Linq;
using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class AuthoredIssueBasisLibraryImpl : IAuthoredIssueBasisLibrary
    {
        private readonly IIssueBasisAutoAuthorLibrary _authors;
        private readonly IIssueBasisLibrary _basises;

        public AuthoredIssueBasisLibraryImpl(IIssueBasisLibrary basises, IIssueBasisAutoAuthorLibrary authors)
        {
            _basises = basises;
            _authors = authors;
        }

        public async Task<AuthoredIssueBasis> FindAsync(IssueId issueId)
        {
            var basis = await _basises.FindAsync(issueId);
            return await CompositeAsync(basis);
        }

        public async Task<AuthoredIssueBasisCollection> AllAsync()
        {
            var basises = await _basises.AllAsync();

            return basises
                .Select(async b => await CompositeAsync(b))
                .Select(r => r.Result)
                .ToCollection();
        }
        
        private async Task<AuthoredIssueBasis> CompositeAsync(IssueBasis basis)
        {
            var author = await _authors.FindByIssueIdAsync(basis.Id);

            return new AuthoredIssueBasis(basis, author);
        }
    }
}