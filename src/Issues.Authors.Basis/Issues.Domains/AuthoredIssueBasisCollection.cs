﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Issues.Domains
{
    public sealed class AuthoredIssueBasisCollection : IEnumerable<AuthoredIssueBasis>
    {
        private readonly IEnumerable<AuthoredIssueBasis> _basises;

        public AuthoredIssueBasisCollection(IEnumerable<AuthoredIssueBasis> basises)
        {
            _basises = basises.ToArray();
        }

        public IEnumerator<AuthoredIssueBasis> GetEnumerator()
        {
            return _basises.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}