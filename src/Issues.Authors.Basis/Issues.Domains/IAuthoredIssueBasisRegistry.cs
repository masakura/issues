﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IAuthoredIssueBasisRegistry
    {
        Task<IssueId> CreateAsync();
    }
}