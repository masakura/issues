﻿using System.Collections.Generic;
using System.Linq;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public static class AuthoredIssueBasisEnumerableExtensions
    {
        public static IEnumerable<IssueId> Ids(this IEnumerable<AuthoredIssueBasis> basises)
        {
            return basises.Select(b => b.Id);
        }

        public static IEnumerable<User> Authors(this IEnumerable<AuthoredIssueBasis> basises)
        {
            return basises.Select(b => b.Author);
        }

        public static AuthoredIssueBasisCollection ToCollection(this IEnumerable<AuthoredIssueBasis> basises)
        {
            return new AuthoredIssueBasisCollection(basises);
        }
    }
}