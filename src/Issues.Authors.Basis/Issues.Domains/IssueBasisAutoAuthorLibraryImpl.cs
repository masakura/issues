﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public sealed class IssueBasisAutoAuthorLibraryImpl : IIssueBasisAutoAuthorLibrary
    {
        private readonly IAutoAuthorLibrary _authors;

        public static ResourceType ResourceType { get; } = (ResourceType) "issue";

        public IssueBasisAutoAuthorLibraryImpl(IAutoAuthorLibrary authors)
        {
            _authors = authors;
        }

        public async Task<User> FindByIssueIdAsync(IssueId issueId)
        {
            return await _authors.FindByResourceIdAsync(ResourceId(issueId), ResourceType);
        }

        private static ResourceId ResourceId(IssueId issueId)
        {
            return (ResourceId) (int) issueId;
        }

        public async Task BindAsync(IssueId issueId)
        {
            await _authors.BindAsync(ResourceId(issueId), ResourceType);
        }
    }
}