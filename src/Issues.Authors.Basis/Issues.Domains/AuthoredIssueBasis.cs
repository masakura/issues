﻿using Issues.Users.Domains;

namespace Issues.Domains
{
    public sealed class AuthoredIssueBasis
    {
        private readonly IssueBasis _basis;

        public AuthoredIssueBasis(IssueBasis basis, User author)
        {
            _basis = basis;
            Author = author;
        }

        public User Author { get; }
        public IssueId Id => _basis.Id;
        public OccurrenceDateTime RegistrationAt => _basis.RegistrationAt;
    }
}