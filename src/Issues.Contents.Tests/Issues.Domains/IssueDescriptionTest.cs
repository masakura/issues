﻿using Xunit;

namespace Issues.Domains
{
    public sealed class IssueDescriptionTest
    {
        [Fact]
        public void CastToString()
        {
            var result = (string) new IssueDescription("hello");
            
            Assert.Equal("hello", result);
        }

        [Fact]
        public void CastFromString()
        {
            Assert.Equal(new IssueDescription("hello"), (IssueDescription) "hello");
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new IssueDescription("hello");
            
            Assert.Equal("hello", (string) result);
        }

        [Fact]
        public void Triming()
        {
            var result = (string) new IssueDescription("  hello  ");
            
            Assert.Equal("hello", result);
        }

        [Fact]
        public void TrimingSpecialCharactors()
        {
            var result = new IssueDescription("  hello \r\n　\t ");
            
            Assert.Equal("hello", (string) result);
        }
    }
}