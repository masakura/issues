﻿using System;
using System.Collections.Generic;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public sealed class Issue : IComparable<Issue>
    {
        private readonly AuthoredIssueBasis _basis;

        public Issue(AuthoredIssueBasis basis, IssueContent content)
        {
            _basis = basis;
            Content = content;
        }

        private static Comparer<Issue> DefaultComparer { get; } = new IssueRelationalComparer();

        public IssueId Id => _basis.Id;
        public User Author => _basis.Author;
        public IssueContent Content { get; }
        public OccurrenceDateTime RegistrationAt => _basis.RegistrationAt;

        public int CompareTo(Issue other)
        {
            return DefaultComparer.Compare(this, other);
        }

        private sealed class IssueRelationalComparer : Comparer<Issue>
        {
            public override int Compare(Issue x, Issue y)
            {
                var result = x.RegistrationAt.CompareTo(y.RegistrationAt);

                if (result > 0) return -1;
                return result < 0 ? 1 : 0;
            }
        }
    }
}