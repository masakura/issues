﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueLibrary
    {
        Task<Issue> FindAsync(IssueId id);
        Task<IssueCollection> AllAsync();
    }
}