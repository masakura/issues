﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class IssueRegistryImpl : IIssueRegistry
    {
        private readonly IAuthoredIssueBasisRegistry _basis;
        private readonly IIssueContentRepository _contents;

        public IssueRegistryImpl(IAuthoredIssueBasisRegistry basis, IIssueContentRepository contents)
        {
            _basis = basis;
            _contents = contents;
        }

        public async Task<IssueId> CreateAsync(IssueContent content)
        {
            var issueId = await _basis.CreateAsync();

            await _contents.CreateAsync(issueId, content);

            return issueId;
        }

        public Task SaveAsync(IssueId id, IssueContent content)
        {
            return _contents.SaveAsync(id, content);
        }
    }
}