﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueRegistry
    {
        Task<IssueId> CreateAsync(IssueContent content);
        Task SaveAsync(IssueId id, IssueContent content);
    }
}