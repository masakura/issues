﻿using Issues;
using Issues.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IssuesServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddIssues(this IServiceCollection services)
        {
            services.AddIssueDomains();
            services.AddIssueBasis();
            services.AddIssueContents();
            services.AddUsers();
            services.AddIssueAuthor();
            services.AddAuthorBasises();

            services.AddTransient<IIssueLibrary, IssueLibraryImpl>();
            services.AddTransient<IIssueRegistry, IssueRegistryImpl>();

            services.AddTransient<IIssueLibraryService, IssueLibraryServiceImpl>();
            services.AddTransient<IIssueRegistrationService, IssueRegistrationServiceImpl>();

            return services;
        }
    }
}