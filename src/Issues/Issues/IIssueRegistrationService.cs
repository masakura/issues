﻿using System.Threading.Tasks;

namespace Issues
{
    public interface IIssueRegistrationService
    {
        Task<IssueId> SubmitAsync(IssueContentViewModel content);
        Task SaveAsync(IssueId id, IssueContentViewModel content);
    }
}