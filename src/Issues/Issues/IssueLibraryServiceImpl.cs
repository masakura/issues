﻿using System.Linq;
using System.Threading.Tasks;
using Issues.Domains;

namespace Issues
{
    public sealed class IssueLibraryServiceImpl : IIssueLibraryService
    {
        private readonly IIssueLibrary _library;

        public IssueLibraryServiceImpl(IIssueLibrary library)
        {
            _library = library;
        }

        public async Task<IssueViewModel> FindAsync(IssueId id)
        {
            return (IssueViewModel) await _library.FindAsync(id);
        }

        public async Task<IssueViewModelCollection> AllAsync()
        {
            var all = await _library.AllAsync();

            return all
                .Select(i => (IssueViewModel) i)
                .ToCollection();
        }
    }
}