﻿using Issues.Domains;
using Issues.Users;

namespace Issues
{
    public sealed class IssueViewModel
    {
        public IssueId Id { get; private set; }
        public IssueContentViewModel Content { get; private set; }
        public OccurrenceDateTime RegistrationAt { get; private set; }
        public UserViewModel Author { get; private set; }

        public static explicit operator IssueViewModel(Issue issue)
        {
            if (issue == null) return null;

            return new IssueViewModel
            {
                Id = issue.Id,
                Content = (IssueContentViewModel) issue.Content,
                RegistrationAt = issue.RegistrationAt,
                Author = (UserViewModel) issue.Author
            };
        }
    }
}