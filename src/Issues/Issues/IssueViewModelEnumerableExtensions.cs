﻿using System.Collections.Generic;

namespace Issues
{
    public static class IssueViewModelEnumerableExtensions
    {
        public static IssueViewModelCollection ToCollection(this IEnumerable<IssueViewModel> issues)
        {
            return new IssueViewModelCollection(issues);
        }
    }
}