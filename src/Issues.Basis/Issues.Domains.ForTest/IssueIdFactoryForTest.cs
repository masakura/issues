﻿using System.Threading.Tasks;

namespace Issues.Domains.ForTest
{
    public static class IssueIdFactoryForTest
    {
        public static IssueId NewId(int id)
        {
            return new IssueId(id);
        }

        public static Task<IssueId> NewIdAsync(int id)
        {
            return Task.FromResult(NewId(id));
        }
    }
}