﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Issues.Domains
{
    public sealed class IssueBasisCollection : IEnumerable<IssueBasis>
    {
        private readonly IEnumerable<IssueBasis> _basises;

        public IssueBasisCollection(IEnumerable<IssueBasis> basises)
        {
            _basises = basises.ToArray();
        }

        public IEnumerator<IssueBasis> GetEnumerator()
        {
            return _basises.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}