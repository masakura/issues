﻿namespace Issues.Domains
{
    public struct IssueBasis
    {
        public IssueId Id { get; }
        public OccurrenceDateTime RegistrationAt { get; }

        public IssueBasis(IssueId id, OccurrenceDateTime registrationAt)
        {
            Id = id;
            RegistrationAt = registrationAt;
        }
    }
}