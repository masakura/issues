﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueBasisRegistry
    {
        Task<IssueId> CreateAsync();
    }
}