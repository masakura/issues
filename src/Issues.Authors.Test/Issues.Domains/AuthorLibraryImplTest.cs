﻿using System;
using System.Threading.Tasks;
using Issues.Users;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Domains
{
    public sealed class AuthorLibraryImplTest
    {
        public AuthorLibraryImplTest()
        {
            _mockAuthors = new Mock<IAuthorRepository>();
            _mockUsers = new Mock<IUserLibrary>();

            _target = new AuthorLibraryImpl(_mockAuthors.Object, _mockUsers.Object);
        }

        private readonly AuthorLibraryImpl _target;
        private readonly Mock<IAuthorRepository> _mockAuthors;
        private readonly Mock<IUserLibrary> _mockUsers;
        private readonly ResourceId _testResourceId = (ResourceId) 5;
        private static readonly UserName TestUser = (UserName) "testuserr";
        private static readonly UserId TestUserId = (UserId) 15;
        private readonly ResourceType _resourceType = new ResourceType("hoge");
        private readonly User _testUser = User.Authenticated(TestUserId, TestUser);

        [Fact]
        public async Task BindAsync()
        {
            _mockAuthors.Setup(x => x.BindAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType)),
                    It.Is<UserId>(p => p.Equals(_testUser.Id))))
                .Returns(Task.CompletedTask);

            await _target.BindAsync(_testResourceId, _resourceType, _testUser);

            _mockAuthors.VerifyAll();
        }

        [Fact]
        public async Task BindAsyncAnonymouse()
        {
            _mockAuthors.Setup(x => x.BindAsync(It.IsAny<ResourceId>(),
                    It.IsAny<ResourceType>(),
                    It.IsAny<UserId>()))
                .Throws(new InvalidOperationException("呼び出されてはいけない"));

            await _target.BindAsync(_testResourceId, _resourceType, User.Anonymouse);
        }

        [Fact]
        public async Task FindByResourceIdAsync()
        {
            _mockAuthors.Setup(x => x.FindByResourcedAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.FromResult(_testUser.Id));
            _mockUsers.Setup(x => x.FindAsync(It.Is<UserId>(p => p.Equals(_testUser.Id))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindByResourceIdAsync(_testResourceId, _resourceType);

            Assert.Equal(_testUser.UserName, result.UserName);
            Assert.False(result.IsAnonymouse);

            _mockAuthors.VerifyAll();
            _mockUsers.VerifyAll();
        }

        [Fact]
        public async Task FindByResourceIdAsyncAnonymouse()
        {
            _mockAuthors.Setup(x => x.FindByResourcedAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.FromResult(UserId.Empty));

            var result = await _target.FindByResourceIdAsync(_testResourceId, _resourceType);

            Assert.True(result.IsAnonymouse);

            _mockAuthors.VerifyAll();
        }

        [Fact]
        public async Task FindByResourceIdAsyncUnknown()
        {
            _mockAuthors.Setup(x => x.FindByResourcedAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.FromResult(TestUserId));
            _mockUsers.Setup(x => x.FindAsync(It.Is<UserId>(p => p.Equals(TestUserId))))
                .Returns(Task.FromResult(User.Unknown));

            var result = await _target.FindByResourceIdAsync(_testResourceId, _resourceType);

            Assert.True(result.IsUnknown);

            _mockAuthors.VerifyAll();
            _mockUsers.VerifyAll();
        }
    }
}