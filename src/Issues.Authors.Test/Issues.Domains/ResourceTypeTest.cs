﻿using Xunit;

namespace Issues.Domains
{
    public sealed class ResourceTypeTest
    {
        [Fact]
        public void CastToString()
        {
            var result = (string) new ResourceType("hello");
            
            Assert.Equal("hello", result);
        }

        [Fact]
        public void CastFromString()
        {
            var result = (ResourceType) "hello";
            
            Assert.Equal((ResourceType) "hello", result);
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new ResourceType("hello").ToString();
            
            Assert.Equal("hello", result);
        }
    }
}