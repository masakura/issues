﻿using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;
using static Issues.Domains.ForTest.IssueBasisFactoryForTest;

namespace Issues.Domains
{
    public class IssueBasisLibraryImplTest
    {
        public IssueBasisLibraryImplTest()
        {
            _mock = new Mock<IIssueBasisRepository>();

            _target = new IssueBasisLibraryImpl(_mock.Object);
        }

        private readonly IssueBasisLibraryImpl _target;
        private readonly Mock<IIssueBasisRepository> _mock;

        [Fact]
        public async Task AllAsync()
        {
            var basises = NewBasises(3).ToCollection();

            _mock.Setup(x => x.AllAsync())
                .Returns(Task.FromResult(basises));

            var result = await _target.AllAsync();

            Assert.Equal(basises.Ids().ToArray(), result.Ids().ToArray());
            Assert.Equal(basises.RegistrationAts().ToArray(),
                result.RegistrationAts().ToArray());
        }

        [Fact]
        public async Task FindAsync()
        {
            var basis = NewBasis(15);

            _mock.Setup(x => x.FindAsync(It.Is<IssueId>(p => p.Equals(basis.Id))))
                .Returns(Task.FromResult(basis));

            var result = await _target.FindAsync((IssueId) 15);

            Assert.Equal(basis.Id, result.Id);
            Assert.Equal(basis.RegistrationAt, result.RegistrationAt);
        }
    }
}