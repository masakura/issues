﻿using System;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace Issues.Domains
{
    public sealed class IssueBasisRegistryImplTest
    {
        public IssueBasisRegistryImplTest()
        {
            _mockBasises = new Mock<IIssueBasisRepository>();
            _mockTimestamp = new Mock<ITimestamp>();

            _target = new IssueBasisRegistryImpl(_mockBasises.Object, _mockTimestamp.Object);
        }

        private readonly IssueBasisRegistryImpl _target;
        private readonly Mock<IIssueBasisRepository> _mockBasises;
        private readonly IssueId _testIssueId = (IssueId) 15;
        private readonly OccurrenceDateTime _registrationAt = (OccurrenceDateTime) DateTime.Now;
        private readonly Mock<ITimestamp> _mockTimestamp;

        [Fact]
        public async Task CreateAsync()
        {
            _mockTimestamp.Setup(x => x.Now())
                .Returns(_registrationAt);
            _mockBasises.Setup(x => x.CreateAsync(It.Is<OccurrenceDateTime>(p => p.Equals(_registrationAt))))
                .Returns(Task.FromResult(_testIssueId));

            var result = await _target.CreateAsync();

            Assert.Equal(15, (int) result);
            
            _mockTimestamp.VerifyAll();
            _mockBasises.VerifyAll();
        }
    }
}