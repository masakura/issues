﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public sealed class AuthorLibraryImpl : IAuthorLibrary
    {
        private readonly IAuthorRepository _authors;
        private readonly IUserLibrary _users;

        public AuthorLibraryImpl(IAuthorRepository authors, IUserLibrary users)
        {
            _authors = authors;
            _users = users;
        }

        public async Task<User> FindByResourceIdAsync(ResourceId resourceId, ResourceType type)
        {
            var userId = await _authors.FindByResourcedAsync(resourceId, type);

            if (userId.IsEmpty) return User.Anonymouse;

            return await _users.FindAsync(userId);
        }

        public async Task BindAsync(ResourceId resourceId, ResourceType type, User user)
        {
            if (user.IsAuthenticated)
                await _authors.BindAsync(resourceId, type, user.Id);
        }
    }
}