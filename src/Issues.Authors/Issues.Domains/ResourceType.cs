﻿namespace Issues.Domains
{
    public struct ResourceType
    {
        private readonly string _value;

        public ResourceType(string value)
        {
            _value = value;
        }

        public static explicit operator string(ResourceType resourceType)
        {
            return resourceType._value;
        }

        public static explicit operator ResourceType(string authorType)
        {
            return new ResourceType(authorType);
        }

        public override string ToString()
        {
            return $"{_value}";
        }
    }
}