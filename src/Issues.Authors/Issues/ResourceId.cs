﻿namespace Issues
{
    public struct ResourceId
    {
        private readonly int _value;

        public ResourceId(int value)
        {
            _value = value;
        }

        public static explicit operator ResourceId(int id)
        {
            return new ResourceId(id);
        }

        public static explicit operator int(ResourceId id)
        {
            return id._value;
        }
    }
}