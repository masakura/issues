﻿using System;
using Issues.Domains;
using Issues.Users.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IssuesAuthorsServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddIssueAuthor(this IServiceCollection services)
        {
            services.AddTransient<IAutoAuthorLibrary>(CreateAutoAuthorLibray);

            return services;
        }

        private static AutoAuthorLibraryImpl CreateAutoAuthorLibray(IServiceProvider services)
        {
            var library = new AuthorLibraryImpl(services.GetService<IAuthorRepository>(), services.GetService<IUserLibrary>());
            return new AutoAuthorLibraryImpl(library, services.GetService<IUserSignInLibrary>());
        }
    }
}