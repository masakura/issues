﻿using System.Threading.Tasks;
using Issues.Users.Domains;
using Moq;
using Xunit;
using static Issues.Users.ForTest.UserFactoryForTest;

namespace Issues.Users
{
    public sealed class UserLibraryServiceImplTest
    {
        private readonly Mock<IUserLibrary> _mock;
        private readonly UserLibraryServiceImpl _target;
        private readonly User _testUser = NewAuthenticated(1);

        public UserLibraryServiceImplTest()
        {
            _mock = new Mock<IUserLibrary>();
            
            _target = new UserLibraryServiceImpl(_mock.Object);
        }

        [Fact]
        public async Task FindByUserNameAsync()
        {
            _mock.Setup(x => x.FindByUserNameAsync(It.Is<UserName>(p => p.Equals(_testUser.UserName))))
                .Returns(Task.FromResult(_testUser));
            
            var result = await _target.FindByUserNameAsync(_testUser.UserName);
            
            Assert.Equal(_testUser.Id, result.Id);
            Assert.Equal(_testUser.UserName, result.UserName);
        }
    }
}