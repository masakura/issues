﻿using Xunit;

namespace Issues.Users
{
    public sealed class ErrorMessageTest
    {
        [Fact]
        public void CastFromString()
        {
            var result = (ErrorMessage) "hello";

            Assert.Equal(new ErrorMessage("hello"), result);
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new ErrorMessage("hello").ToString();

            Assert.Equal("hello", result);
        }

        [Fact]
        public void CastToString()
        {
            var result = (string) new ErrorMessage("hello");

            Assert.Equal("hello", result);
        }
    }
}