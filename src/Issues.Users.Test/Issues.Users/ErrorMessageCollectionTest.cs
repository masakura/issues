﻿using Xunit;

namespace Issues.Users
{
    public sealed class ErrorMessageCollectionTest
    {
        [Fact]
        public void ConvertToString()
        {
            var result = new ErrorMessageCollection(new[]
            {
                (ErrorMessage) "error1",
                (ErrorMessage) "error2",
                (ErrorMessage) "error3"
            });
            
            Assert.Equal("error1\nerror2\nerror3", result.ToString());
        }

        [Fact]
        public void HasError()
        {
            var result = new ErrorMessageCollection(new[]
            {
                (ErrorMessage) "error"
            });

            Assert.True(result.HasErrors);
        }

        [Fact]
        public void NoError()
        {
            var result = new ErrorMessageCollection(new ErrorMessage[] { });

            Assert.False(result.HasErrors);
        }
    }
}