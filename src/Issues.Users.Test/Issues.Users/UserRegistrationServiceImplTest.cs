﻿using System.Threading.Tasks;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Users
{
    public sealed class UserRegistrationServiceImplTest
    {
        private readonly Mock<IUserRepository> _mock;
        private readonly UserRegistrationServiceImpl _target;

        public UserRegistrationServiceImplTest()
        {
            _mock = new Mock<IUserRepository>();

            _target = new UserRegistrationServiceImpl(_mock.Object);
        }

        [Fact]
        public async Task RegisterAsync()
        {
            var userName = (UserName) "foo@example.com";
            var password = (Password) "wefoih32_A";

            _mock.Setup(x => x.CreateAsync(userName, password))
                .Returns(Task.FromResult(ErrorMessageCollection.NoError));
            
            await _target.RegisterAsync((UserName) "foo@example.com", (Password) "wefoih32_A");
        }
    }
}