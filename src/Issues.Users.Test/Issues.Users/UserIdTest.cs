﻿using Xunit;

namespace Issues.Users
{
    public sealed class UserIdTest
    {
        [Fact]
        public void CastFromInt()
        {
            var result = (UserId) 15;

            Assert.Equal(new UserId(15), result);
        }

        [Fact]
        public void CastToInt()
        {
            var result = (int) new UserId(15);

            Assert.Equal(15, result);
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new UserId(15).ToString();

            Assert.Equal("15", result);
        }
    }
}