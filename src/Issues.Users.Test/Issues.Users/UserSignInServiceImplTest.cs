﻿using System.Threading.Tasks;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Users
{
    public sealed class UserSignInServiceImplTest
    {
        public UserSignInServiceImplTest()
        {
            _mock = new Mock<IUserRepository>();

            _target = new UserSignInServiceImpl(_mock.Object);

            _testUser = (UserName) "testuser";
            _testPassword = (Password) "password";
        }

        private readonly Mock<IUserRepository> _mock;
        private readonly UserSignInServiceImpl _target;
        private readonly UserName _testUser;
        private readonly Password _testPassword;

        [Fact]
        public async Task SignInAsync()
        {
            _mock.Setup(x => x.SignInAsync(It.Is<UserName>(p => p.Equals(_testUser)),
                    It.Is<Password>(p => p.Equals(_testPassword))))
                .Returns(Task.FromResult(true));

            var result = await _target.SignInAsync(_testUser, _testPassword);

            Assert.False(result.HasErrors);

            _mock.VerifyAll();
        }

        [Fact]
        public async Task SignInAsyncLoginFailed()
        {
            _mock.Setup(x => x.SignInAsync(It.Is<UserName>(p => p.Equals(_testUser)),
                    It.Is<Password>(p => p.Equals(_testPassword))))
                .Returns(Task.FromResult(false));

            var result = await _target.SignInAsync(_testUser, _testPassword);

            Assert.True(result.HasErrors);
            Assert.Equal("ユーザー名またはパスワードが異なります。", $"{result}");

            _mock.VerifyAll();
        }
    }
}