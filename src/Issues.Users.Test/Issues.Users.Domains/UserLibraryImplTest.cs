﻿using System.Threading.Tasks;
using Moq;
using Xunit;
using static Issues.Users.ForTest.UserFactoryForTest;

namespace Issues.Users.Domains
{
    public sealed class UserLibraryImplTest
    {
        public UserLibraryImplTest()
        {
            _mock = new Mock<IUserRepository>();

            _target = new UserLibraryImpl(_mock.Object);
        }

        private readonly UserLibraryImpl _target;
        private readonly Mock<IUserRepository> _mock;
        private readonly User _testUser = NewAuthenticated(1);

        [Fact]
        public async Task FindAsync()
        {
            _mock.Setup(x => x.FindAsync(It.Is<UserId>(p => p.Equals(_testUser.Id))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindAsync(_testUser.Id);

            Assert.Equal(_testUser.Id, result.Id);
            Assert.Equal(_testUser.UserName, result.UserName);
        }

        [Fact]
        public async Task FindByUserNameAsync()
        {
            _mock.Setup(x => x.FindByUserNameAsync(It.Is<UserName>(p => p.Equals(_testUser.UserName))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindByUserNameAsync(_testUser.UserName);

            Assert.Equal(_testUser.Id, result.Id);
            Assert.Equal(_testUser.UserName, result.UserName);
        }
    }
}